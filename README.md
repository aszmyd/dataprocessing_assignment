dataprocessing_assignment
=========================

Project providing data processing middleware.

Requirements
====================
* PHP >= 5.3
* Gearman Job Server (http://gearman.org/) - I've used **1.1.12** version
* MySQL5 Server


Installation
====================
After cloning the repo install dependencies with Composer for app and for GearmanUI.

    composer install ; composer --working-dir=gearman-ui install
    
If you dont have your database yet, create it with command:

    php app/console doctrine:database:create
    
Update schema:

    php app/console doctrine:schema:update --force

Clean Gearman workers cache 'just in case':

    php app/console gearman:cache:warmup ;

**And you're ready to go!**

Usage - Workers
====================
Application provides few type of workers:

* **Processor**
* **Deduplicator**
* **Archiver**

Each one could be started from console like this:

    php app/console gearman:worker:execute AppBundleWorkersProcessor
    php app/console gearman:worker:execute AppBundleWorkersDeduplicator
    php app/console gearman:worker:execute AppBundleWorkersArchiver

If You see an error when trying to run Workers like:

    [GearmanException]              
    Failed to set exception option

Then You probably don't have Gearman Daemon running. 

You can test if your Gearman is running by executing such command:

    (echo status ; sleep 0.1) | nc 127.0.0.1 4730 -w 1

If You see `Connection refused.` then Gearman **IS NOT** running.

Usage - GearmanUI
====================
After starting build in symfony2 server:

    php app/console server:run
    
Go (default host and port) there: http://127.0.0.1:8000/gearman-ui/ and you should see GearmanUI interface

Debug - Fake Data provider
====================
There is also an Command to provide fake data. This command:

    php app/console fakedata --limit=5 --interval=3
    
Will send some 5 fake data items in every 3 seconds.