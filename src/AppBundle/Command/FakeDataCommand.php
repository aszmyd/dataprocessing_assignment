<?php
/**
 * Created by PhpStorm.
 * User: aszmyd
 * Date: 3/13/15
 * Time: 8:10 AM
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FakeDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fakedata')
            ->setDescription('Send Fake data to Gearman worker')
            ->addOption(
                'worker',
                null,
                InputOption::VALUE_OPTIONAL,
                'Worker and job where you want to send data.',
                'AppBundleWorkersProcessor~process'
            )
            ->addOption(
                'interval',
                null,
                InputOption::VALUE_OPTIONAL,
                'Interval in seconds between each fake data send.',
                2
            )
            ->addOption(
                'limit',
                null,
                InputOption::VALUE_OPTIONAL,
                'Maximum fake data chunks to send at each iteration. Zero means no limit - data will be send forever',
                0
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $worker = $input->getOption('worker');
        $interval = $input->getOption('interval');
        $limit = $input->getOption('limit');

        $gearman = $this
            ->getContainer()
            ->get('gearman');

        $sended = 0;
        while ($limit === 0 || $sended < $limit) {

            $gearman
                ->doBackgroundJob($worker, $this->generateFakeItem());

            $sended++;

            $output->writeln('Sended ' . $sended . ' chunk of data.');

            sleep($interval);
        }

    }


    /**
     * @return string JSON encoded data
     */
    private function generateFakeItem()
    {

        $item = array(

            "id" => "333",
            "syncBag" => "bag1",
            "grade" => "B",
            "currentData" => array(
                "id" => "data3",
                "status" => "-1",
                "created" => "2014-09-07T12:41:04+00:00",
                "dataString0" => "http://example.com/file.html",
                "dataString1" => "2014-02-07T12:40:14+00:00",
                "dataString2" => "ExampleCategory",
                "dataString3" => "1234AB",
                "dataString4" => "35asd5asd2a1sd545",
                "dataString5" => "123"
            )
        );

        return json_encode($item);

    }
}