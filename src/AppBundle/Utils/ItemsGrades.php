<?php

namespace AppBundle\Utils;


use Symfony\Component\Config\Definition\Exception\Exception;

class ItemsGrades {

    public static $grades = array(
        'A', 'B', 'C', 'D', 'E', 'F'
    );

    private static $dataStringGrades = array(
        'missing' => array(
            '0' => 'F',
            '1' => 'F',
            '2' => 'B',
            '3' => 'F',
            '4' => 'B',
            '5' => 'B',
        ),
        'invalid' => array(
            '0' => 'D',
            '1' => 'C',
            '2' => 'B',
            '3' => 'C',
            '4' => 'B',
            '5' => 'B',
        ),
        'valid' => array(
            '0' => 'A',
            '1' => 'A',
            '2' => 'A',
            '3' => 'A',
            '4' => 'A',
            '5' => 'A',
        ),
    );

    /**
     * Check if first given grade is heigher than second
     *
     * @param $firstGrade string
     * @param $secondGrade string
     * @return boolean
     */
    public static function isFirstGradeHigherThanSecond($firstGrade, $secondGrade) {

        $firstIndex = array_search($firstGrade, self::$grades);
        $secondIndex = array_search($secondGrade, self::$grades);

        if($firstIndex === false) {
            throw new Exception('No such grade: ' . $firstGrade);
        }

        if($secondGrade === false) {
            throw new Exception('No such grade: ' . $secondGrade);
        }

        return $firstIndex < $secondIndex;

    }

    /**
     * Calculate grade for value and return it
     *
     * @param $stringDataIndex int
     * @param $value string
     * @return string
     */
    public static function getGradeOfStringData($stringDataIndex, $value) {

        if(!$value) {
            $bag = 'missing';
        } elseif(!self::isValidStringData($stringDataIndex, $value)) {
            $bag = 'invalid';
        } else {
            $bag = 'valid';
        }

        return self::$dataStringGrades[$bag][$stringDataIndex];

    }
    /**
     * Check if string data is valid
     *
     * @param $stringDataIndex int
     * @param $value string
     * @return string
     */
    private static function isValidStringData($stringDataIndex, $value) {

        $validatorClassName = '\AppBundle\Validator\DataString' . $stringDataIndex . 'Validator';

        /** @var \AppBundle\Validator\DataStringValidator $validatorClass */
        $validatorClass = new $validatorClassName();
        return $validatorClass->isValid($value);

    }

}