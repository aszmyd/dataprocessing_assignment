<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Item;
use AppBundle\Entity\ItemData;

class ItemsTransformations {

    /**
     * Mark current data as discarted and save change in history
     * @param $item array
     * @return array
     */
    public static function makeItemDataDiscardedAndReturnIt($item) {

        $item = self::addHistoryStateFromCurrentData($item);

        $item['currentData']['status'] = ItemData::STATUS_DISCARDER;

        return $item;

    }

    /**
     * Merge donor current data item into master current data (get highest-rated data from donor and put it inside master).
     * @param $donorCurrentData array
     * @param $masterCurrentData array
     * @return array
     */
    public static function mergeDonorCurrentDataIntoMasterCurrentDataAndReturn($donorCurrentData, $masterCurrentData) {

        $mergedData = $masterCurrentData;

        for($index = 0 ; $index < ItemData::DATA_STRING_NUMBER ; $index++) {

            $fieldName = 'dataString' . $index;

            $masterValueGrade = ItemsGrades::getGradeOfStringData($index, $masterCurrentData[$fieldName]);
            $donorValueGrade = ItemsGrades::getGradeOfStringData($index, $donorCurrentData[$fieldName]);

            if(ItemsGrades::isFirstGradeHigherThanSecond($donorValueGrade, $masterValueGrade)) {
                $mergedData[$fieldName] = $donorCurrentData[$fieldName];
            }
        }

        return $mergedData;

    }

    /**
     * Add currentData as history entry
     * @param $item array
     * @return  array
     */
    public static function addHistoryStateFromCurrentData($item) {

        if(!isset($item['historyData'])) {
            $item['historyData'] = array();
        }

        $dataToAdd = $item['currentData'];
        $dataToAdd['status'] = ItemData::STATUS_DISCARDER;
        $item['historyData'][] = $dataToAdd;

        return $item;

    }

}