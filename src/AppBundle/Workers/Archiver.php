<?php

namespace AppBundle\Workers;

use Mmoreram\GearmanBundle\Driver\Gearman;
use AppBundle\Entity\Item;
use AppBundle\Entity\ItemData;


/**
 * @Gearman\Work(
 *     service = "worker.archiver",
 *     iterations = 0,
 *     description = "Worker archive items in database",
 *     servers = {
 *         { "host": "127.0.0.1", "port": 4730 },
 *     }
 * )
 */
class Archiver extends AppWorker
{

    /**
     * Method to store data in databae
     *
     * @param \GearmanJob $job Object with job parameters
     *
     * @return boolean
     *
     * @Gearman\Job(
     *     description = "Store items in database"
     * )
     */
    public function archive(\GearmanJob $job)
    {

        $syncItem = json_decode($job->workload(), true);

        try {

            $this->processArchiving($syncItem);

        } catch (\Exception $e) {
            $this->output->writeln(sprintf('<error>Error processing item (id: %s): %s</error>', $syncItem['id'], $e->getMessage()));
        }


        return true;
    }

    /**
     * Helper to actually make the archiving
     *
     * @param $syncItem
     * @throws \Exception
     */
    private function processArchiving($syncItem)
    {

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->container->get("doctrine")->getManager();

        /** @var \Doctrine\ORM\EntityRepository $itemRespository */
        $itemRespository = $em->getRepository('AppBundle:Item');

        /** @var \AppBundle\Entity\Item $entity */
        $itemEntity = $itemRespository->findOneByItemId($syncItem['id']);

        if (!$itemEntity) {
            $itemEntity = new Item();
            $em->persist($itemEntity);
        }

        // Set basic info about item
        $itemEntity->setSyncBag($syncItem['syncBag']);
        $itemEntity->setItemId($syncItem['id']);
        $itemEntity->setGrade($syncItem['grade']);

        // Set currentData
        if (isset($syncItem['currentData'])) {

            $currentDataEntity = $itemEntity->getCurrentData();
            if (!$currentDataEntity) {
                $currentDataEntity = new ItemData();
                $em->persist($currentDataEntity);
            }

            $currentDataEntity->setItemDataId($syncItem['currentData']['id']);
            $currentDataEntity->setStatus($syncItem['currentData']['status']);
            $currentDataEntity->setCreated(new \DateTime($syncItem['currentData']['created']));
            $currentDataEntity->setDataString0($syncItem['currentData']['dataString0']);
            $currentDataEntity->setDataString1($syncItem['currentData']['dataString1']);
            $currentDataEntity->setDataString2($syncItem['currentData']['dataString2']);
            $currentDataEntity->setDataString3($syncItem['currentData']['dataString3']);
            $currentDataEntity->setDataString4($syncItem['currentData']['dataString4']);
            $currentDataEntity->setDataString5($syncItem['currentData']['dataString5']);

            $itemEntity->setCurrentData($currentDataEntity);
        }

        // Set historyData
        if (isset($syncItem['historyData']) && is_array($syncItem['historyData'])) {

            $historyDataEntities = $itemEntity->getHistoryData();
            foreach ($itemEntity->getHistoryData() as $historyDataEntity) {
                $itemEntity->removeHistoryDatum($historyDataEntity);
            }

            foreach ($syncItem['historyData'] as $historyData) {

                $historyDataEntity = new ItemData();

                $historyDataEntity->setItemDataId($historyData['id']);
                $historyDataEntity->setStatus($historyData['status']);
                $historyDataEntity->setCreated(new \DateTime($historyData['created']));
                $historyDataEntity->setDataString0($historyData['dataString0']);
                $historyDataEntity->setDataString1($historyData['dataString1']);
                $historyDataEntity->setDataString2($historyData['dataString2']);
                $historyDataEntity->setDataString3($historyData['dataString3']);
                $historyDataEntity->setDataString4($historyData['dataString4']);
                $historyDataEntity->setDataString5($historyData['dataString5']);
                $historyDataEntity->setItem($itemEntity);

                $em->persist($historyDataEntity);
                $historyDataEntities->add($historyDataEntity);

            }
        }

        $em->flush();

        $this->output->writeln('Save in DB ' . $syncItem['id'] . ' | Database row id: ' . $itemEntity->getId());
    }

}