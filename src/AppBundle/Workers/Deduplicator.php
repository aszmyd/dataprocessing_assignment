<?php

namespace AppBundle\Workers;

use AppBundle\Utils\ItemsGrades;
use AppBundle\Utils\ItemsTransformations;
use Mmoreram\GearmanBundle\Driver\Gearman;

/**
 * @Gearman\Work(
 *     service = "worker.deduplicator",
 *     iterations = 0,
 *     description = "Worker find duplicates of items and process them",
 *     servers = {
 *         { "host": "127.0.0.1", "port": 4730 },
 *     }
 * )
 */
class Deduplicator extends AppWorker
{

    /**
     * Method to deduplicate items
     *
     * @param \GearmanJob $job Object with job parameters
     *
     * @return boolean
     *
     * @Gearman\Job(
     *     description = "Find duplicates of item and process them"
     * )
     */
    public function deduplicate(\GearmanJob $job)
    {

        $syncItem = json_decode($job->workload(),true);

        try {

            $this->processDeduplication($syncItem);

        } catch(\Exception $e) {
            $this->output->writeln(sprintf('<error>Error processing item (id: %s): %s</error>', $syncItem['id'], $e->getMessage()));
        }


        return true;
    }

    /**
     * Helper to actually looking for duplications of item
     *
     * @param $syncItem
     * @throws \Exception
     */
    private function processDeduplication($syncItem) {

        $this->output->writeln(sprintf('Finding duplicates of %s', $syncItem['id']));

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->container->get("doctrine")->getManager();

        /** @var \AppBundle\Entity\ItemRepository $itemRespository */
        $itemRespository = $em->getRepository('AppBundle:Item');

        $existingItem = $itemRespository->findItemByIdAndReturnAsArray($syncItem['id']);

        if(!$existingItem) {

            $this->output->writeln('<info>Item ' . $syncItem['id'] . ' has NO duplications. Just pass it to Archiver</info>');

            // There are no duplicates, just store this item in db
            $this->gearman->doBackgroundJob('AppBundleWorkersArchiver~archive', json_encode($syncItem));

        } else {

            $this->output->writeln('<info>Found duplicated item: ' . $existingItem['id'] . '</info>');

            if(ItemsGrades::isFirstGradeHigherThanSecond($syncItem['grade'], $existingItem['grade'])) {
                $master = $syncItem;
                $donor = $existingItem;
            } else {
                $master = $existingItem;
                $donor = $syncItem;
            }

            $this->output->writeln('-- Master item grade: '.$master['grade']);
            $this->output->writeln('-- Donor item grade: '.$donor['grade']);

            // Merge donor into master
            $mergedData = ItemsTransformations::mergeDonorCurrentDataIntoMasterCurrentDataAndReturn($donor['currentData'], $master['currentData']);
            if($mergedData !== $existingItem['currentData']) {
                $existingItem = ItemsTransformations::addHistoryStateFromCurrentData($existingItem);
                $existingItem['currentData'] = $mergedData;
            }


            // Out base will be existing entity in db so it will have grade from master and currentData from merged array
            $existingItem['grade'] = $master['grade'];
            $existingItem['syncBag'] = $master['syncBag']; // TODO: should it work like that? we override syncBag

            $this->output->writeln('-- Pass existing item after merge into Archive');

            // Store it in db
            $this->gearman->doBackgroundJob('AppBundleWorkersArchiver~archive', json_encode($existingItem));

        }


    }

}