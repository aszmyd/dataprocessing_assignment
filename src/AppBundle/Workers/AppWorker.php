<?php

namespace AppBundle\Workers;

use AppBundle\Entity\ItemData;
use Mmoreram\GearmanBundle\Command\Util\GearmanOutputAwareInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mmoreram\GearmanBundle\Service\GearmanClient;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\DependencyInjection\Container;

abstract class AppWorker implements GearmanOutputAwareInterface
{

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var GearmanClient
     */
    protected $gearman;

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }


    /**
     * Constructor
     */
    public function __construct(Container $container)
    {
        $this->output = new NullOutput();
        $this->container = $container;

        $this->gearman = $container->get('gearman');

    }

    /**
     * Helper to fill all fields that are optional with null so that we dont need to check everywhere in the app if specific field exists
     * @param $itemData
     * @throws \Exception
     */
    protected function fillOptionalFieldsAndReturnItemData($itemData) {

        for($dataStringIndex = 0 ; $dataStringIndex <= ItemData::DATA_STRING_NUMBER ; $dataStringIndex++) {

            /** @var \AppBundle\Validator\DataStringValidator  $validatorClassName */
            $validatorClassName = '\AppBundle\Validator\DataString' . $dataStringIndex . 'Validator';

            $isRequiredField = $validatorClassName::isRequired();
            $fieldName = $validatorClassName::getFieldName();

            if(isset($itemData[$fieldName])) {
                continue;
            }

            if($isRequiredField) {
                throw new \Exception(sprintf('Missing at least one required field in item data: %s', $fieldName));
            } else {
                $itemData[$fieldName] = $validatorClassName::getDefaultValue();
            }
        }

        return $itemData;

    }


}