<?php

namespace AppBundle\Workers;

use Mmoreram\GearmanBundle\Driver\Gearman;

use AppBundle\Entity\Item;
use AppBundle\Utils\ItemsTransformations;

/**
 * @Gearman\Work(
 *     service = "worker.processor",
 *     iterations = 0,
 *     description = "Worker to receive data items and process them",
 *     servers = {
 *         { "host": "127.0.0.1", "port": 4730 },
 *     }
 * )
 */
class Processor extends AppWorker
{

    /**
     * Method to process data
     *
     * @param \GearmanJob $job Object with job parameters
     *
     * @return boolean
     *
     * @Gearman\Job(
     *     description = "Process received queue of items"
     * )
     */
    public function process(\GearmanJob $job)
    {

        $syncItem = json_decode($job->workload(), true);

        try {

            $this->processProcess($syncItem);

        } catch (\Exception $e) {
            $this->output->writeln(sprintf('<error>Error processing item (id: %s): %s</error>', $syncItem['id'], $e->getMessage()));
        }

        return true;

    }

    /**
     * Helper to actually make the process action
     *
     * @param $syncItem
     * @throws \Exception
     */
    private function processProcess($syncItem)
    {

        $syncItem['currentData'] = $this->fillOptionalFieldsAndReturnItemData($syncItem['currentData']);

        $grade = $syncItem['grade'];

        if ($grade == Item::GRADETO_DISCARD) {

            // Store useless data in db
            $this->output->writeln(sprintf('Item %s is <comment>useless</comment>. Set status as -1 and send it to Archiver', $syncItem['id']));

            $item = ItemsTransformations::makeItemDataDiscardedAndReturnIt($syncItem);

            $this->gearman
                ->doBackgroundJob('AppBundleWorkersArchiver~archive', json_encode($item));

        } else {
            // Process data
            $this->output->writeln(sprintf('Item %s it <info>useable</info>. Send it to Deduplicator', $syncItem['id']));

            $this->gearman
                ->doBackgroundJob('AppBundleWorkersDeduplicator~deduplicate', json_encode($syncItem));

        }

    }

}