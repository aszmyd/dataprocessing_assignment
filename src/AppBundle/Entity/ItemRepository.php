<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;


class ItemRepository extends EntityRepository
{
    /**
     * Find item by itemId and and return result as array
     * @param $itemId
     * @return array|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findItemByIdAndReturnAsArray($itemId)
    {

        $em = $this->getEntityManager();

        $query = $em->createQueryBuilder('i')
            ->select('i')
            ->from('AppBundle:Item', 'i')
            ->where('i.itemId = :itemId')
            ->setParameter('itemId', $itemId)
            ->leftJoin('i.currentData', 'currentData')->addSelect('currentData')
            ->leftJoin('i.historyData', 'historyData')->addSelect('historyData')
            ->getQuery();

        $result = $query->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        $resultWithIdInsteadOfItemId = $this->changeItemIdToIdAndReturn($result);
        return $this->formatDateTimeObjectsAndReturn($resultWithIdInsteadOfItemId);

    }

    /**
     * Helper to change 'itemId' and 'itemDataId' for ids
     *
     * @param $itemArray
     * @return array
     */
    private function changeItemIdToIdAndReturn($itemArray) {

        if(!empty($itemArray)) {
            $itemArray['id'] = $itemArray['itemId'];
            unset($itemArray['itemId']);

            $itemArray['currentData']['id'] = $itemArray['currentData']['itemDataId'];
            unset($itemArray['currentData']['itemDataId']);

            if(isset($itemArray['historyData']) && is_array($itemArray['historyData'])) {
                for($i = 0 ; $i < count($itemArray['historyData']) ; $i++) {
                    $itemArray['historyData'][$i]['id'] = $itemArray['historyData'][$i]['itemDataId'];
                    unset($itemArray['historyData'][$i]['itemDataId']);
                }
            }
        }

        return $itemArray;
    }

    /**
     * Helper to format all DateTime objects to simply strings
     *
     * @param $itemArray
     * @param $format Format to use in $date->format method of DateTime object
     * @return array
     */
    private function formatDateTimeObjectsAndReturn($itemArray, $format = 'c') {

        if(!empty($itemArray)) {

            $itemArray['currentData']['created'] = $itemArray['currentData']['created']->format($format);

            if(isset($itemArray['historyData']) && is_array($itemArray['historyData'])) {
                for($i = 0 ; $i < count($itemArray['historyData']) ; $i++) {
                    $itemArray['historyData'][$i]['created'] = $itemArray['historyData'][$i]['created']->format($format);
                }
            }
        }

        return $itemArray;
    }

}