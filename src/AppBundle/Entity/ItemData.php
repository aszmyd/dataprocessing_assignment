<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ItemData
{

    /**
     * Data Statuses
     */
    const STATUS_DISCARDER = '-1';
    const STATUS_TO_PROCESS = '0';
    const STATUS_READONLY = '1';

    /**
     * Number od data strings
     */
    const DATA_STRING_NUMBER = 6;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(name="item_data_id", type="string", length=128)
     */
    protected $itemDataId;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="historyData")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     **/
    private $item;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    protected $status;


    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;


    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $dataString0;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $dataString1;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $dataString2;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $dataString3;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $dataString4;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $dataString5;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ItemData
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ItemData
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set dataString0
     *
     * @param string $dataString0
     * @return ItemData
     */
    public function setDataString0($dataString0)
    {
        $this->dataString0 = $dataString0;

        return $this;
    }

    /**
     * Get dataString0
     *
     * @return string 
     */
    public function getDataString0()
    {
        return $this->dataString0;
    }

    /**
     * Set dataString1
     *
     * @param string $dataString1
     * @return ItemData
     */
    public function setDataString1($dataString1)
    {
        $this->dataString1 = $dataString1;

        return $this;
    }

    /**
     * Get dataString1
     *
     * @return string 
     */
    public function getDataString1()
    {
        return $this->dataString1;
    }

    /**
     * Set dataString2
     *
     * @param string $dataString2
     * @return ItemData
     */
    public function setDataString2($dataString2)
    {
        $this->dataString2 = $dataString2;

        return $this;
    }

    /**
     * Get dataString2
     *
     * @return string 
     */
    public function getDataString2()
    {
        return $this->dataString2;
    }

    /**
     * Set dataString3
     *
     * @param string $dataString3
     * @return ItemData
     */
    public function setDataString3($dataString3)
    {
        $this->dataString3 = $dataString3;

        return $this;
    }

    /**
     * Get dataString3
     *
     * @return string 
     */
    public function getDataString3()
    {
        return $this->dataString3;
    }

    /**
     * Set dataString4
     *
     * @param string $dataString4
     * @return ItemData
     */
    public function setDataString4($dataString4)
    {
        $this->dataString4 = $dataString4;

        return $this;
    }

    /**
     * Get dataString4
     *
     * @return string 
     */
    public function getDataString4()
    {
        return $this->dataString4;
    }

    /**
     * Set dataString5
     *
     * @param string $dataString5
     * @return ItemData
     */
    public function setDataString5($dataString5)
    {
        $this->dataString5 = $dataString5;

        return $this;
    }

    /**
     * Get dataString5
     *
     * @return string 
     */
    public function getDataString5()
    {
        return $this->dataString5;
    }

    /**
     * Get all dataStrings as key => value. Example: [dataString0 => value0, dataString1 => value1 etc.]
     *
     * @return array
     */
    public function getDataStrings()
    {
        $result = array();
        for($i = 0 ; $i < self::DATA_STRING_NUMBER ; $i++) {
            $fieldName = 'dataString' . $i;
            $result[$fieldName] = $this->{$fieldName};
        }
        return $result;
    }

    /**
     * Set itemDataId
     *
     * @param string $itemDataId
     * @return ItemData
     */
    public function setItemDataId($itemDataId)
    {
        $this->itemDataId = $itemDataId;

        return $this;
    }

    /**
     * Get itemDataId
     *
     * @return string 
     */
    public function getItemDataId()
    {
        return $this->itemDataId;
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Item $item
     * @return ItemData
     */
    public function setItem(\AppBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Item 
     */
    public function getItem()
    {
        return $this->item;
    }
}
