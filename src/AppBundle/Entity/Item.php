<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ItemRepository")
 */
class Item
{

    /**
     * Which grade needs an item to have to be considered as useless and its data discarded
     */
    const GRADETO_DISCARD = 'F';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="item_id", type="string", length=128)
     */
    protected $itemId;


    /**
     * @ORM\Column(name="sync_bag", type="string", length=128)
     */
    protected $syncBag;


    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $grade;


    /**
     * @ORM\OneToOne(targetEntity="ItemData")
     * @ORM\JoinColumn(name="current_data", referencedColumnName="id")
     **/
    protected $currentData;

    /**
     * @ORM\OneToMany(targetEntity="ItemData", mappedBy="item", orphanRemoval=true)
     * @ORM\JoinColumn(name="history_data", referencedColumnName="id")
     **/
    protected $historyData;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemId
     *
     * @param string $itemId
     * @return Item
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return string 
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set syncBag
     *
     * @param string $syncBag
     * @return Item
     */
    public function setSyncBag($syncBag)
    {
        $this->syncBag = $syncBag;

        return $this;
    }

    /**
     * Get syncBag
     *
     * @return string 
     */
    public function getSyncBag()
    {
        return $this->syncBag;
    }

    /**
     * Set grade
     *
     * @param string $grade
     * @return Item
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set currentData
     *
     * @param \AppBundle\Entity\ItemData $currentData
     * @return Item
     */
    public function setCurrentData(\AppBundle\Entity\ItemData $currentData = null)
    {
        $this->currentData = $currentData;

        return $this;
    }

    /**
     * Get currentData
     *
     * @return \AppBundle\Entity\ItemData 
     */
    public function getCurrentData()
    {
        return $this->currentData;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->historyData = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add historyData
     *
     * @param \AppBundle\Entity\ItemData $historyData
     * @return Item
     */
    public function addHistoryDatum(\AppBundle\Entity\ItemData $historyData)
    {
        $this->historyData[] = $historyData;

        return $this;
    }

    /**
     * Remove historyData
     *
     * @param \AppBundle\Entity\ItemData $historyData
     */
    public function removeHistoryDatum(\AppBundle\Entity\ItemData $historyData)
    {
        $this->historyData->removeElement($historyData);
    }

    /**
     * Get historyData
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoryData()
    {
        return $this->historyData;
    }
}
