<?php

namespace AppBundle\Validator;

abstract class DataStringValidator implements DataStringValidatorInterface
{

    /**
     * @var bool
     */
    protected static $isRequired = true;

    /**
     * @var null
     */
    protected static $defaultValue = null;

    /**
     * @return bool
     */
    public static function isRequired()
    {
        return static::$isRequired;
    }

    /**
     * @return mixed
     */
    public static function getFieldName()
    {
        return static::$fieldName;
    }

    /**
     * @return null
     */
    public static function getDefaultValue()
    {
        return static::$defaultValue;
    }

}