<?php

namespace AppBundle\Validator;

/**
 * Validator - must contain at least 4 numbers and 2 characters
 * Class DataString3Validator
 * @package AppBundle\Validator
 */
class DataString3Validator extends DataStringValidator {

    /**
     * @var bool
     */
    static $fieldName = 'dataString3';

    public function isValid($value)
    {
        $chars = preg_match_all('/[a-z]/i', $value);
        $numbers = preg_match_all('/\d/', $value);

        return $chars >= 2 && $numbers >= 4;
    }
}