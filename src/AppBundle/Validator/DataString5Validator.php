<?php

namespace AppBundle\Validator;

/**
 * Validator - must be an integer or double with no significant decimals (only zeroes)
 * Class DataString5Validator
 * @package AppBundle\Validator
 */
class DataString5Validator extends DataStringValidator
{

    /**
     * @var bool
     */
    static $fieldName = 'dataString5';

    /**
     * This field is optional
     * @var bool
     */
    static $isRequired = false;

    public function isValid($value)
    {

        $validNumber = preg_match_all('/[^\d]/', $value) === 0;

        $doubleVal = doubleval($value);
        $doubleValWithNoDecimals = doubleval(intval($value));

        return $validNumber && $doubleVal === $doubleValWithNoDecimals;

    }
}