<?php

namespace AppBundle\Validator;

/**
 * Validator - must be interpretable to a DateTime object
 * Class DataString1Validator
 * @package AppBundle\Validator
 */
class DataString1Validator extends DataStringValidator
{
    /**
     * @var bool
     */
    static $fieldName = 'dataString1';

    public function isValid($value)
    {
        try {
            new \DateTime($value);
            return true;
        } catch (\Exception $ex) {
            return false;
        }

    }
}