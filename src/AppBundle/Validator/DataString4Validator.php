<?php

namespace AppBundle\Validator;

/**
 * Validator - a string containing only printable characters. Must be between 5 and 40 characters
 * Class DataString4Validator
 * @package AppBundle\Validator
 */
class DataString4Validator extends DataStringValidator
{
    /**
     * @var bool
     */
    static $fieldName = 'dataString4';

    /**
     * This field is optional
     * @var bool
     */
    static $isRequired = false;

    public function isValid($value)
    {
        $length = strlen($value);
        return ctype_print($value) && $length >= 5 && $length <= 40;
    }
}