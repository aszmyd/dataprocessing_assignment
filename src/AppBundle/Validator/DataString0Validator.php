<?php

namespace AppBundle\Validator;

/**
 * Validator - must be an URL
 * Class DataString0Validator
 * @package AppBundle\Validator
 */
class DataString0Validator extends DataStringValidator {

    /**
     * @var bool
     */
    static $fieldName = 'dataString0';

    public function isValid($value)
    {
        return filter_var($value, FILTER_VALIDATE_URL) !== false;
    }
}