<?php

namespace AppBundle\Validator;

interface DataStringValidatorInterface {
    public function isValid($value);
    public static function isRequired();
    public static function getFieldName();
}