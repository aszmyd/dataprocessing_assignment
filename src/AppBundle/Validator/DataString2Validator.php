<?php

namespace AppBundle\Validator;

/**
 * Validator - a string containing only printable characters
 * Class DataString2Validator
 * @package AppBundle\Validator
 */
class DataString2Validator extends DataStringValidator {

    /**
     * @var bool
     */
    static $fieldName = 'dataString2';

    /**
     * This field is optional
     * @var bool
     */
    static $isRequired = false;

    public function isValid($value)
    {
        return ctype_print($value);
    }
}