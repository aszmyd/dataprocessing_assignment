<?php

namespace AppBundle\Validator;

/**
 * TODO: how exacly should this be validated?
 * Validator - It must only occur once
 * Class DataString5Validator
 * @package AppBundle\Validator
 */
class DataString6Validator extends DataStringValidator {

    /**
     * @var bool
     */
    static $fieldName = 'dataString6';

    /**
     * This field is optional
     * @var bool
     */
    static $isRequired = false;

    public function isValid($value)
    {
        return true;
    }
}